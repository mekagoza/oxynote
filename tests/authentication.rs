extern crate actix;
extern crate actix_web;
extern crate dotenv;
extern crate envy;
extern crate futures;
extern crate oxynote;
#[macro_use]
extern crate serde_json;
#[macro_use]
extern crate fake;

use actix_web::client::{ClientRequestBuilder, ClientResponse};
use actix_web::http::StatusCode as Status;
use actix_web::test::TestServer;
use actix_web::{http, HttpMessage};
use futures::future::Future;

const ORIGIN: &'static str = "test-client";

fn setup() -> TestServer {
    dotenv::from_filename("test.env").ok();

    let raw: oxynote::ConfigRaw = envy::from_env().unwrap();
    let cfg = oxynote::Config::from_raw(&raw);

    TestServer::with_factory(move || oxynote::build(&cfg))
}

fn authorize_req(builder: &mut ClientRequestBuilder, resp: &ClientResponse) {
    let cookies = resp.cookies().expect("Bad cookies");

    for cookie in cookies {
        builder.cookie(cookie.clone());
    }
}

#[test]
fn register() {
    let mut server = setup();

    // Register
    let pass = fake!(Internet.password(8, 64));

    let formdata = json!({
            "username" : fake!(Internet.user_name),
            "password" : pass,
            "confirm": pass,
            "email": fake!(Internet.safe_email)
        });

    let req = server
        .client(http::Method::POST, "/auth/register")
        .header("Origin", ORIGIN)
        .json(formdata)
        .expect("req");

    let response = server.execute(req.send()).expect("resp");

    assert_eq!(response.status(), Status::OK);

    let mut builder = server.client(http::Method::GET, "/auth/refresh");

    authorize_req(&mut builder, &response);

    let req = builder.header("Origin", ORIGIN).finish().expect("req");

    let response = server.execute(req.send()).expect("resp");

    assert_eq!(response.status(), Status::OK);

    let body = response.body().wait().expect("Bad body");
    assert!(!body.is_empty());
}

#[test]
fn register_and_login() {
    let mut server = setup();

    // Register
    let (uname, pass) = (fake!(Internet.user_name), fake!(Internet.password(8, 64)));
    let formdata = json!({
            "username" : &uname,
            "password" : &pass,
            "confirm": &pass,
            "email": fake!(Internet.safe_email)
        });

    let req = server
        .client(http::Method::POST, "/auth/register")
        .header("Origin", ORIGIN)
        .json(formdata)
        .expect("req");

    let response = server.execute(req.send()).expect("resp");

    assert_eq!(response.status(), Status::OK);

    // Check Login
    let formdata = json!({
        "username" : &uname,
        "password" : &pass,
    });

    let req = server
        .client(http::Method::POST, "/auth/login")
        .header("Origin", ORIGIN)
        .json(formdata)
        .expect("req");

    let response = server.execute(req.send()).expect("resp");

    assert_eq!(response.status(), Status::OK);

    // Check auth
    let mut builder = server.client(http::Method::GET, "/auth/refresh");

    authorize_req(&mut builder, &response);

    let req = builder.header("Origin", ORIGIN).finish().expect("req");

    let response = server.execute(req.send()).expect("resp");

    assert_eq!(response.status(), Status::OK);

    let body = response.body().wait().expect("Bad body");
    assert!(!body.is_empty());
}
