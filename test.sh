#!/bin/sh -e
# Test script lifted in large parts from https://github.com/hashbang/userdb
# Copyright © 2017-2018 Keller Fuchs <kellerfuchs@hashbang.sh>

run() {
	normal='\e[0m'
	yellow='\e[33m'
	printf "${yellow}%s${normal}\n" "$*" >&2

	"$@"
}

# Some distros support multiple installed versions of PostgreSQL
if ! command -v initdb 2>/dev/null; then
	for dir in /usr/lib/postgresql/*; do
		export PATH="${dir}/bin:${PATH}"
	done
fi

if ! command -v initdb 2>/dev/null; then
	echo "No PostgreSQL utilities in PATH" >&2
	exit 1
fi

# Cleanup on exit
trap 'pg_ctl -D "${WORKDIR}" stop; rm -rf -- "${WORKDIR}"' EXIT
WORKDIR=$(mktemp -d)

# Start a PostgreSQL server
run initdb -D "${WORKDIR}"
run pg_ctl -D "${WORKDIR}" start -w -o "  \
	-c unix_socket_directories=${WORKDIR}   \
	-c listen_addresses='127.0.0.1'         \
"

# Setup the environment
export PGHOST=${WORKDIR}
export RUST_BACKTRACE=1

# Create database
export PGDATABASE="${USER}"
run createdb "${USER}" # TODO: Fix database name

# Setup postgre extensions and schema
run psql -v ON_ERROR_STOP=1 <<EOF
create extension pgcrypto;
EOF
for schema in migrations/*/up.sql; do
	run psql -v ON_ERROR_STOP=1 -f "$schema"
done

# Create .env file
cat > .env <<EOF
DB_URL=postgresql://localhost
LOG_PATH=${PWD}/log
HOST=localhost
PORT=8080
PASS_KEY=$(head -c 32 /dev/urandom | base64 )
SESS_KEY=$(head -c 32 /dev/urandom | base64 )
EOF

# Run command!
if [ -z "$@" ]; then
	run cargo test
else
	run "$@"
fi
