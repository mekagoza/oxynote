use chrono::{DateTime, Utc};
use uuid::Uuid;

use model::schema::notes;
use model::User;

#[derive(Debug, Clone, Serialize, PartialEq, Queryable, Identifiable, Associations)]
#[table_name = "notes"]
#[belongs_to(User, foreign_key = "author")]
#[belongs_to(Note, foreign_key = "parent")]
pub struct Note {
    pub id: Uuid,
    pub author: Uuid,
    pub body: String,
    pub parent: Option<Uuid>,
    pub archived: bool,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Deserialize, Insertable)]
#[table_name = "notes"]
pub struct NewNote<'a> {
    pub author: Uuid,
    pub body: &'a str,
    pub parent: Option<Uuid>,
}
