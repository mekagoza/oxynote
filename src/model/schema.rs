table! {
    notes (id) {
        id -> Uuid,
        author -> Uuid,
        body -> Text,
        parent -> Nullable<Uuid>,
        archived -> Bool,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    tokens (id) {
        id -> Uuid,
        client -> Text,
        value -> Text,
        created_at -> Timestamptz,
        last_used -> Timestamptz,
        user_id -> Uuid,
    }
}

table! {
    users (id) {
        id -> Uuid,
        username -> Text,
        password -> Text,
        email -> Text,
        verified -> Bool,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

joinable!(notes -> users (author));
joinable!(tokens -> users (user_id));

allow_tables_to_appear_in_same_query!(
    notes,
    tokens,
    users,
);
