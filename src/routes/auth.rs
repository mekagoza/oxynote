use actix_web::http;
use actix_web::middleware::session::Session;
use actix_web::{AsyncResponder, FutureResponse, HttpMessage, HttpRequest, Json, State};
use futures::Future;

use core::auth;
use error::Error;
use model::{Token, User};
use SfsApp;

pub fn register(
    (state, sess, form, req): (
        State<SfsApp>,
        Session,
        Json<forms::Register>,
        HttpRequest<SfsApp>,
    ),
) -> FutureResponse<Json<User>, Error> {
    let ua = req.headers()
        .get(http::header::USER_AGENT)
        .map(|hv| hv.to_str().unwrap_or(""))
        .unwrap_or("unknown")
        .to_owned();

    let form = form.into_inner();

    let msg = auth::Register {
        username: form.username,
        password: form.password,
        confirm: form.confirm,
        email: form.email,
        client: ua,
    };

    state
        .db
        .send(msg)
        .from_err()
        .and_then(move |res| {
            let (user, token) = res?;

            let uid = user.id;

            sess.set("user_id", uid)?;
            sess.set("token", token.value)?;

            Ok(Json(user))
        })
        .responder()
}

pub fn login(
    (state, sess, form, req): (
        State<SfsApp>,
        Session,
        Json<forms::Login>,
        HttpRequest<SfsApp>,
    ),
) -> FutureResponse<Json<User>, Error> {
    let ua = req.headers()
        .get(http::header::USER_AGENT)
        .map(|hv| hv.to_str().unwrap_or(""))
        .unwrap_or("unknown")
        .to_owned();

    let form = form.into_inner();

    let msg = auth::Login {
        username: form.username,
        password: form.password,
        client: ua,
    };

    state
        .db
        .send(msg)
        .from_err()
        .and_then(move |res| {
            let (user, token) = res?;

            let uid = user.id;

            sess.set("user_id", uid)?;
            sess.set("token", token.value)?;

            Ok(Json(user))
        })
        .responder()
}

pub fn change_pw(
    (state, _, sess, form, req): (
        State<SfsApp>,
        Token,
        Session,
        Json<forms::ChangePw>,
        HttpRequest<SfsApp>,
    ),
) -> FutureResponse<&'static str, Error> {
    let ua = req.headers()
        .get(http::header::USER_AGENT)
        .map(|hv| hv.to_str().unwrap_or(""))
        .unwrap_or("")
        .to_owned();

    let form = form.into_inner();

    let msg = auth::ChangePw {
        username: form.username,
        password: form.password,
        current_password: form.current_password,
        client: ua,
    };

    state
        .db
        .send(msg)
        .from_err()
        .and_then(move |res| {
            let token = res?;
            sess.set("token", token.value)?;

            Ok("success")
        })
        .responder()
}

pub fn refresh(
    (sess, token, state): (Session, Token, State<SfsApp>),
) -> FutureResponse<&'static str, Error> {
    let msg = auth::Refresh { token };

    state
        .db
        .send(msg)
        .from_err()
        .and_then(move |res| {
            let ref_token = res?;

            sess.set("token", ref_token.value)?;

            Ok("success")
        })
        .responder()
}

mod forms {
    #[derive(Deserialize)]
    pub struct Login {
        pub username: String,
        pub password: String,
    }

    #[derive(Deserialize)]
    pub struct Register {
        pub username: String,
        pub email: String,
        pub password: String,
        pub confirm: String,
    }

    #[derive(Deserialize)]
    pub struct ChangePw {
        pub username: String,
        pub current_password: String,
        pub password: String,
    }
}
