extern crate actix;
extern crate actix_web;
extern crate argon2;
extern crate base64;
extern crate chrono;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate failure;
extern crate futures;
extern crate hex;
extern crate r2d2;
extern crate rand;
extern crate ring;
extern crate serde;
#[macro_use]
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate slog;
extern crate slog_async;
extern crate slog_term;
extern crate uuid;

mod core;
mod error;
mod log;
mod model;
mod pass;
mod routes;
mod token;

use actix::prelude::*;
use actix_web::middleware::cors::Cors;
use actix_web::middleware::session::{CookieSessionBackend, SessionStorage};
use actix_web::App;
use slog::Logger;

use core::{init_pool, Core, PgPool};
use routes::auth as au;

#[derive(Debug, Deserialize, Clone)]
pub struct ConfigRaw {
    pub db_url: String,
    pub log_path: String,
    pub host: String,
    pub port: u16,
    pub pass_key: String,
    pub sess_key: String,
}

#[derive(Debug, Clone)]
pub struct Config {
    pub db_url: String,
    pub log_path: String,
    pub host: String,
    pub port: u16,
    pub pass_key: Vec<u8>,
    pub sess_key: Vec<u8>,
}

impl Config {
    pub fn from_raw(raw: &ConfigRaw) -> Config {
        Config {
            db_url: raw.db_url.clone(),
            log_path: raw.log_path.clone(),
            host: raw.host.clone(),
            port: raw.port,
            pass_key: base64::decode(&raw.pass_key).expect("Bad base64 pass key"),
            sess_key: base64::decode(&raw.sess_key).expect("Bad base64 session key"),
        }
    }
}

// #[derive(Clone)]
pub struct SfsApp {
    #[allow(unused)]
    logger: Logger,
    db: Addr<Syn, Core>,
    core: Core,
}

pub fn build(config: &Config) -> App<SfsApp> {
    let db_pool = match init_pool(&config.db_url) {
        Ok(pool) => pool,
        Err(e) => panic!("Error initializing database pool: {}", e),
    };

    let logger = log::log(&config.log_path);

    let cors = cors();

    let hasher = pass::Hasher::new(&config.pass_key);

    let backend = CookieSessionBackend::private(&config.sess_key)
        .domain(config.host.as_str())
        .path("/")
        .name("oxynote_session")
        .max_age(chrono::Duration::hours(1));
    let session = SessionStorage::new(backend);

    build_app(session, db_pool, cors, logger, hasher)
}

fn build_app(
    session: SessionStorage<CookieSessionBackend, SfsApp>,
    pool: PgPool,
    cors: Cors,
    logger: Logger,
    hasher: pass::Hasher,
) -> App<SfsApp> {
    let core_logger = logger.new(o!("context" => "Core"));
    let extra = Core::new(core_logger.clone(), pool.clone(), hasher.clone());

    let addr = SyncArbiter::start(4, move || {
        Core::new(core_logger.clone(), pool.clone(), hasher.clone())
    });

    let state = SfsApp {
        db: addr,
        logger: logger.clone(),
        core: extra,
    };

    App::with_state(state)
        .middleware(log::LogMiddleware(logger))
        .middleware(session)
        .middleware(cors)
        .resource("/auth/login", |r| r.post().with(au::login))
        .resource("/auth/register", |r| r.post().with(au::register))
        .resource("/auth/refresh", |r| r.get().with(au::refresh))
        .resource("/auth/change_pw", |r| r.post().with(au::change_pw))
}

fn cors() -> Cors {
    Cors::build().send_wildcard().max_age(3600).finish()
}
