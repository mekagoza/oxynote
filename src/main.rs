extern crate actix;
extern crate actix_web;
extern crate dotenv;
extern crate envy;
extern crate oxynote;

use actix_web::server;

fn main() {
    let raw: oxynote::ConfigRaw = envy::from_env().expect("Couldn't load config from environment");
    let cfg = oxynote::Config::from_raw(&raw);
    println!("config = {:#?}", raw);

    let sys = actix::System::new("oxynote");

    server::new(move || oxynote::build(&cfg))
        .bind((raw.host.as_str(), raw.port))
        .expect("Error launching Actix Server")
        .start();

    let _ = sys.run();
}
