use actix::prelude::*;
use base64;
use diesel;
use diesel::prelude::*;

use super::Core;
use error::{Error, Result};
use model::{NewToken, NewUser, Token, User};

fn encode<Bytes: ?Sized + AsRef<[u8]>>(bytes: &Bytes) -> String {
    base64::encode_config(bytes, base64::URL_SAFE)
}

#[derive(Debug)]
pub struct Login {
    pub username: String,
    pub password: String,
    pub client: String,
}

pub struct Refresh {
    pub token: Token,
}

pub struct ChangePw {
    pub username: String,
    pub current_password: String,
    pub password: String,
    pub client: String,
}

pub struct Register {
    pub username: String,
    pub email: String,
    pub password: String,
    pub confirm: String,
    pub client: String,
}

impl Message for Login {
    type Result = Result<(User, Token)>;
}

impl Message for Register {
    type Result = Result<(User, Token)>;
}

impl Message for ChangePw {
    type Result = Result<Token>;
}

impl Message for Refresh {
    type Result = Result<Token>;
}

impl Handler<Login> for Core {
    type Result = Result<(User, Token)>;

    fn handle(&mut self, login_user: Login, _: &mut Self::Context) -> Result<(User, Token)> {
        use model::schema::tokens;
        use model::schema::users::dsl::*;

        let conn = self.conn()?;

        let (name, pass) = (&login_user.username, &login_user.password);
        let user: Option<User> = users.filter(username.eq(name)).first(&conn).optional()?;

        if let Some(u) = user {
            if self.hasher.verify(pass, &u.password)? {
                let tok = self.hasher.gen_token();
                let tok = encode(&tok);

                let new_token = NewToken {
                    client: &login_user.client,
                    user_id: u.id,
                    value: &tok,
                };

                let token = diesel::insert_into(tokens::table)
                    .values(&new_token)
                    .get_result(&conn)?;

                return Ok((u, token));
            } else {
                error!(self.log, "password verification failed");
            }
        } else {
            error!(self.log, "no such user found");
        }

        error!(self.log, "error authorizing user");
        return Err(Error::Unauthorized("Username/Password Unknown".into()));
    }
}

impl Handler<Register> for Core {
    type Result = Result<(User, Token)>;

    fn handle(&mut self, reg: Register, _: &mut Self::Context) -> Result<(User, Token)> {
        use model::schema::tokens;
        use model::schema::users::dsl::*;

        if reg.confirm != reg.password {
            return Err(Error::BadInput("Passwords don't match"));
        }

        let conn = self.conn()?;

        let hash = self.hasher.hash(&reg.password);

        let new_user = NewUser {
            email: &reg.email,
            password: &hash,
            username: &reg.username,
        };

        info!(self.log, "registering user {}", new_user.username,);

        let u: User = diesel::insert_into(users)
            .values(&new_user)
            .get_result(&conn)?;

        let tok = self.hasher.gen_token();
        let tok = encode(&tok);

        let new_token = NewToken {
            client: &reg.client,
            user_id: u.id,
            value: &tok,
        };

        let token = diesel::insert_into(tokens::table)
            .values(&new_token)
            .get_result(&conn)?;

        Ok((u, token))
    }
}

impl Handler<ChangePw> for Core {
    type Result = Result<Token>;

    fn handle(&mut self, cp: ChangePw, _: &mut Self::Context) -> Result<Token> {
        use model::schema::tokens;
        use model::schema::users::dsl::*;

        let conn = self.conn()?;

        let user: Option<User> = users
            .filter(username.eq(&cp.username))
            .first(&conn)
            .optional()?;

        let err = Err(Error::Unauthorized("Username/Password Unknown".into()));

        let user = match user {
            Some(u) => u,
            None => {
                error!(self.log, "error authorizing user");
                return err;
            }
        };

        if !self.hasher.verify(&cp.current_password, &user.password)? {
            error!(self.log, "error authorizing user");
            return err;
        }

        let new_hash = self.hasher.hash(&cp.password);

        diesel::update(&user)
            .set(password.eq(new_hash))
            .execute(&conn)?;

        info!(self.log, "Changed password for user {}", cp.username);

        let tok = self.hasher.gen_token();
        let tok = encode(&tok);

        let new_token = NewToken {
            client: &cp.client,
            user_id: user.id,
            value: &tok,
        };

        // Delete all old tokens
        diesel::delete(tokens::table.filter(tokens::user_id.eq(user.id))).execute(&conn)?;

        let token = diesel::insert_into(tokens::table)
            .values(&new_token)
            .get_result(&conn)?;

        Ok(token)
    }
}

impl Handler<Refresh> for Core {
    type Result = Result<Token>;

    fn handle(&mut self, msg: Refresh, _: &mut Self::Context) -> Result<Token> {
        use model::schema::tokens;

        let conn = self.conn()?;

        let token: Token = tokens::table.find(msg.token.id).first(&conn)?;

        Ok(token)
    }
}
