pub mod auth;

use actix::prelude::*;
use diesel::prelude::*;
use diesel::r2d2::{ConnectionManager, Pool, PooledConnection};
use slog::Logger;

use error::Result;
use pass::Hasher;

pub struct Core {
    pool: PgPool,
    log: Logger,
    hasher: Hasher,
}

impl Core {
    pub fn new(logger: Logger, pool: PgPool, hasher: Hasher) -> Core {
        Core {
            log: logger,
            pool,
            hasher,
        }
    }

    pub fn conn(&self) -> Result<PooledConnection<ConnectionManager<PgConnection>>> {
        match self.pool.get() {
            Ok(conn) => Ok(conn),
            Err(e) => Err(e.into()),
        }
    }
}

impl Actor for Core {
    type Context = SyncContext<Self>;
}

pub type PgPool = Pool<ConnectionManager<PgConnection>>;

#[allow(dead_code)]
pub type Conn = PooledConnection<ConnectionManager<PgConnection>>;

pub fn init_pool(db_url: &str) -> Result<PgPool> {
    let manager = ConnectionManager::<PgConnection>::new(db_url);
    let pool = Pool::new(manager)?;
    Ok(pool)
}
