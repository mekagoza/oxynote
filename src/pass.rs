use argon2::{self, Config};
use rand::rngs::StdRng;
use rand::{FromEntropy, RngCore};

const SALT_LEN: usize = 32;

#[derive(Clone)]
pub struct Hasher {
    config: Config,
    rng: StdRng,
}

impl Hasher {
    pub fn new(_key: &[u8]) -> Hasher {
        let c = Config::default();

        // TODO gotta wait until bugfix
        // c.secret = key.to_vec();

        Hasher {
            rng: StdRng::from_entropy(),
            config: c,
        }
    }

    pub fn hash(&mut self, password: &str) -> String {
        // Salt
        let mut salt = vec![0; SALT_LEN];
        self.rng.fill_bytes(&mut salt);
        let salt = salt;

        // Build Hash
        argon2::hash_encoded(password.as_bytes(), &salt, &self.config)
            .expect("Hashing should never fail")
    }

    pub fn verify(&self, password: &str, stored: &String) -> Result<bool, argon2::Error> {
        argon2::verify_encoded(stored, password.as_bytes())
    }

    pub fn gen_token(&mut self) -> Vec<u8> {
        let mut vec = vec![0; 32];
        self.rng.fill_bytes(&mut vec);

        vec
    }
}
